#Zein Gualsaquí
#Programa para detectar si un número es primo 

def numpri (num):
    if num<2:
        return False 
    for i in range(2, num):
        if num % i ==0:
            return False
        else:
            return True

def categorizacion ():
    num= int(input("Ingresa el número:\n"))
    resultado= numpri(num)

    if resultado is True:
        print ("El número es primo")

    else:
        print ("El número no es primo")

if __name__=='__main__':
    categorizacion()
